FROM openjdk:20-jdk-slim

COPY build/libs/ /app/

WORKDIR /app

CMD ["java", "-jar", "demo-0.0.1-SNAPSHOT.jar"]