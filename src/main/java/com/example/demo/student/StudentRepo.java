package com.example.demo.student;

import org.springframework.stereotype.Repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;


@Repository
public interface StudentRepo extends JpaRepository<Student, Long> {
    Optional<Student> findStudentByEmail(String email);
}
