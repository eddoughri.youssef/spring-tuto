package com.example.demo.student;

// spring boot
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

// java core
import java.time.LocalDate;
import java.time.Month;
import java.util.List;

@Configuration
public class StudentConfig {
    
    @Bean
    CommandLineRunner commandLineRunner(StudentRepo studentRepo){
        return args -> {
            Student s1 = new Student( 1L, "Mariam", "user@gmail.com", LocalDate.of(2000, Month.JANUARY, 5) );
            Student s2 = new Student( 2L, "Mariam", "user@gmail.com", LocalDate.of(2002, Month.JANUARY, 5) );
            Student s3 = new Student( 3L, "Mariam", "user@gmail.com", LocalDate.of(2003, Month.JANUARY, 5) );
            Student s4 = new Student( 4L, "Mariam", "user@gmail.com", LocalDate.of(2004, Month.JANUARY, 5) );
            Student s5 = new Student( 5L, "Mariam", "user@gmail.com", LocalDate.of(2005, Month.JANUARY, 5) );
            studentRepo.saveAll( List.of(s1, s2, s3, s4, s5) );
        };
    }
}
