package com.example.demo.student;

import org.springframework.web.bind.annotation.DeleteMapping;
// spring boot
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;

// java
import java.util.List;


@RestController
@RequestMapping(path = "api/v1/class/students")
public class StudentController {
    private final StudentService studentService;

    // constructors
    @Autowired
    public StudentController(StudentService studentService){
        this.studentService = studentService;
    }

	@GetMapping
	public List<Student> getStudents(){
        return studentService.getStudents();    
	}

    @PostMapping
    public String registerNewStudent(@RequestBody Student student){
        try{
            studentService.addNewStudent(student);
        }catch(IllegalStateException e){
            System.out.println(e.getMessage());
            return e.getMessage();
        }
        return "student added";
    }

    @DeleteMapping(path = "{studentId}")
    public String deleteStudent(@PathVariable("studentId") Long studentId){
        try{
            studentService.deleteStudent(studentId);
        }catch(IllegalStateException e){
            System.out.println(e.getMessage());
            return e.getMessage();
        }
        return "student deleted";
    }

    @PutMapping(path = "{studentId}")
    public String updateStudent(@PathVariable("studentId") Long studentId, @RequestParam( required = false ) String name, @RequestParam( required = false ) String email){
        try{
            studentService.updateStudent(studentId, name, email);
        }catch(IllegalStateException e){
            System.out.println(e.getMessage());
            return e.getMessage();
        }
        return "student updated";
    }
}
