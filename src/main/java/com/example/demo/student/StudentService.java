package com.example.demo.student;

// springboot
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jakarta.transaction.Transactional;

import java.util.Optional;

// java 8
// import java.time.LocalDate;
// import java.time.Month;
import java.util.List;

@Service
public class StudentService {
    
	private final StudentRepo studentRepo;

	@Autowired
	public StudentService(StudentRepo studentRepo){
		this.studentRepo = studentRepo;
	}

    public List<Student> getStudents(){
		return studentRepo.findAll();
    }

	public void addNewStudent(Student student){
		// do this email exists in the database:
		Optional<Student> studentByEmail = studentRepo.findStudentByEmail(student.getEmail());
		if(studentByEmail.isPresent()){
			throw new IllegalStateException("email taken");
		}
		studentRepo.save(student);
	}

	public void deleteStudent(Long studentId){
		// does this student exists in the database:
		boolean studentById = studentRepo.existsById(studentId);
		if(!studentById){
			throw new IllegalStateException("student does not exist");
		}
		studentRepo.deleteById(studentId);
	}

	@Transactional
	public void updateStudent(Long studentId, String name, String email){
		// does this student exists in the database:
		boolean studentById = studentRepo.existsById(studentId);
		if(!studentById){
			throw new IllegalStateException("student does not exist");
		}
		Student student = studentRepo.findById(studentId).get();
		if(name != null && name.length() > 0 && !student.getName().equals(name)){
			student.setName(name);
		}
		if(email != null && email.length() > 0 && !student.getEmail().equals(email)){
			// do this email exists in the database:
			student.setEmail(email);
		}
		studentRepo.save(student);
	}
}